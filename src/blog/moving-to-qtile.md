# Moving to Qtile
#### 2023-06-02
I decided to give up on [xmonad][1]. Honestly, I just needed something to do.

Moving to Qtile wasn't half as arduous as I thought it would be, and within a day I had a config I was happy with. I spent most of that time working out how best to sort out workspaces to work with multiple screens the way I like it, ending up with four workspaces per screen, bound to Mod+1-4 for the primary screen and Mod+F1-4 for the secondary.

Python is my primary language and configuring Qtile in it was a much smoother experience than my fucking about with Haskell. I wrote my own [bar widget][1] to display my currently playing song as a programming exercise more than anything else. Configuring the bar in Python was much preferred to fucking about with xmobar. It even has a systray! 

Switching to a Python-based WM also allowed me to appreciate how much I ***absolutely fucking hate*** Haskell tooling, especially on Arch. I had to compile xmonad, xmonad-contrib and xmobar through stack because the Arch packages were dynamically compiled, which always took bloody ages. I could never get GHCi working properly in my xmonad config which made testing changes harder. I'm sure these are *solvable* issues, but I could not be fucking bothered so I just left it.

Probably the only arduous bit of configuring Qtile was group management. I ended up writing a lambda to return which key I needed to use depending on the group name and a very long ternary expression to make group labels (which is useless because they don't show on the bar anyway).

So yeah, I'm happy with my decision to switch. For now, at least.
  
[1]: https://xmonad.org
[2]: https://codeberg.org/the-boy-sebastian/qtile-config/src/branch/main/widget.py